

$(document).ready(function() {

    PaymentSession.configure({
        // Please provide the MPGS SESSION_ID that you have created at the backend. MPGS will provision a new SESSION_ID ifyou leave it empty
        // session: SESSION_ID
        fields: {
            // these should match the ids of the input controls defined in section 2.3
            card: {
                number: "#card-number",
                securityCode: "#security-code",
                expiryMonth: "#expiry-month",
                expiryYear: "#expiry-year",
                nameOnCard: "#cardholder-name"
            }
        },
        frameEmbeddingMitigation: ["javascript"],
        callbacks: {
            initialized: function(response) {
                // This callback will be triggered once MPGS session is initialized.
                if (response.status !== "ok") {
                    console.error(response);
                }
            },
            formSessionUpdate: function(response) {
                // This callback will be triggered once the session is updated with payment card information.
                if (response.status) {
                if ("ok" == response.status) {
                    // The MPGS session is updated with the payment card information successfully.
                    // You may redirect to your endpoint specified in section 3.3
                    var sessionId = response.session.id;
                    // This is for illustration purpose, you may append the sessionId in the url or retrieve it from the endpoint.
                    window.location.href = BEGIN_PAYMENT_ENDPOINT + sessionId;
                } else if ("fields_in_error" == response.status) {
                        // This is an error handling section, you are advised to display error message on the payment page when the inputted fields encountered error.
                        console.log("Session update failed with field errors.");
                        
                        if (response.errors.cardNumber) {
                            console.log("Card number is invalid or missing.");
                        }
                        if (response.errors.expiryYear) {
                            console.log("Expiry year is invalid or missing.");
                        }
                        if (response.errors.expiryMonth) {
                            console.log("Expiry month is invalid or missing.");
                        }
                        if (response.errors.securityCode) {
                            console.log("Security code is invalid.");
                        }
                        } else if ("request_timeout" == response.status) {
                            console.log("Session update failed with request timeout: " + response.errors.message);
                        } else if ("system_error" == response.status) {
                            console.log("Session update failed with system error: " + response.errors.message);
                        }
                    } else {
                    console.log("Session update failed: " + response);
                }   
                }
            },
            interaction: {
                displayControl: {
                    formatCard: "EMBOSSED",
                    invalidFieldCharacters: "REJECT"
                }
            }
        });


        var paymentButton = document.getElementById("paymentButton");
        paymentButton.addEventListener("click", pay);   
});

function pay() {
    // Update the MPGS session with the input from hosted fields
    PaymentSession.updateSessionFromForm("card");
}