# Gateway CSharp Sample Code
This is a sample application to help developers start building CSharp applications.

##Important information
This project is initially built in Visual Studio for Mac 7.3.3 and .NET CLI on Mac.


## Prerequisites 
1. dotnet core 2.1.4+
2. Registered account with MPGS Gateway system

## Authentication
1. You can authenticate in one of two ways:
- Using your API key and password (available from the merchant portal). To do this, see instructions below. The following fields are required for password authentication: merchant ID, API password, currency, and gateway base URL.
- Using a certificate, which can be downloaded from the merchant portal. To do this, see instructions [here](CERT_AUTH.md). Certificate authentication is not supported on MacOS Platform.

## Steps for running locally
1. Download code
2. Set the environment variables

    - On Mac/Linux: Use the ```export``` command:

            prompt> export GATEWAY_MERCHANT_ID=YOUR_MERCHANT_ID 
            prompt> export GATEWAY_API_PASSWORD=YOUR_API_PASSWORD
            prompt> export GATEWAY_BASE_URL=YOUR_GATEWAY_BASE_URL
            prompt> export GATEWAY_CURRENCY=YOUR_CURRENCY (optional - default is USD)
            prompt> export GATEWAY_VERSION=YOUR_VERSION (optional - default is version 45)

    - On Windows, use the ```set``` command:

            prompt> set GATEWAY_MERCHANT_ID=YOUR_MERCHANT_ID
            prompt> set GATEWAY_API_PASSWORD=YOUR_API_PASSWORD
            prompt> set GATEWAY_BASE_URL=YOUR_GATEWAY_BASE_URL
            prompt> set GATEWAY_CURRENCY=YOUR_CURRENCY (optional - default is USD)
            prompt> set GATEWAY_VERSION=YOUR_VERSION (optional - default is version 45)

3. Run the following:

dotnet build
dotnet run 

4. Navigate to *http://localhost:5000* to test locally

## Disclaimer
This software is intended for **TEST/REFERENCE** purposes **ONLY** and is not intended to be used in a production environment.


5.MacOS users

## Using environments variables in applications started by Visual Studio
By default App launch by finder don't inherance the system variables. To using the system variables information, Visual Studio must be launched by terminal:

/Applications/Visual\ Studio.app/Contents/MacOS/VisualStudio &


##SSL on MacOS

Keychain and SSL certificates isn't supported so far on dotnet core 2.0 for Mac OS Sierra High (Libcrurl issue : https://github.com/dotnet/corefx/issues/9728).


