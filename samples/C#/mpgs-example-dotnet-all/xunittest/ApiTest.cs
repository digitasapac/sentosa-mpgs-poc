using System;
using System.Collections.Generic;
using gateway_csharp_sample_code.Controllers;
using gateway_csharp_sample_code.Gateway;
using gateway_csharp_sample_code.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using Moq.Language;
using Xunit;

namespace gateway_csharp_sample_code.xunittest
{

    /// <summary>
    /// Bundle package test
    /// </summary>
    public class ApiTest : BaseTest
    {

        private String PAGE_API_VIEW =  "~/Views/Payment/ApiResponse.cshtml";
        private String PAGE_RECEIPT = "~/Views/Payment/Receipt.cshtml";
        private String PAGE_SECURE_ID = "~/Views/Payment/SecureIdPayerAuthenticationForm.cshtml";
        private String PAGE_ERROR = "Error";


        private String PAGE_MASTERPASS_BUTTON  = "~/Views/Payment/MasterpassButton.cshtml";




        public ApiTest()
        {
        }

        protected PaymentApiController getPaymentApiController(List<String> MockListResponse)
        {
            var ioptions = new Mock<IOptions<GatewayApiConfig>>();
            ioptions.Setup(op => op.Value).Returns(getGatewayApiConfig());

            var loggerGatewayApi = new Mock<ILogger<gateway_csharp_sample_code.Gateway.GatewayApiClient>>();
            var loggerNVPClient = new Mock<ILogger<gateway_csharp_sample_code.Gateway.NVPApiClient>>();
            var loggerPaymentApiController = new Mock<ILogger<gateway_csharp_sample_code.Controllers.PaymentApiController>>();


            var gatewayApiClient = new Mock<GatewayApiClient>(ioptions.Object, loggerGatewayApi.Object);
            var setupApiClient = gatewayApiClient.SetupSequence(o => o.SendTransaction(It.IsNotNull<GatewayApiRequest>()));
            ISetupSequentialResult<string> returnChain = null;
            foreach(String mockResponse in MockListResponse){
                if(returnChain == null) {
                    returnChain = setupApiClient.Returns(mockResponse);
                } else {
                    returnChain.Returns(mockResponse);    
                }
            }

            var nvpApiClient = new Mock<NVPApiClient>(ioptions.Object, loggerNVPClient.Object);
            nvpApiClient.Setup(n => n.SendTransaction(It.IsNotNull<GatewayApiRequest>())).Returns(MockListResponse[0]);

            var paymentApiController =  new PaymentApiController(ioptions.Object, gatewayApiClient.Object, nvpApiClient.Object, loggerPaymentApiController.Object);
            paymentApiController.ControllerContext = getControllerContext();

            return paymentApiController;
        }

        protected PaymentApiController getPaymentApiController(String MockResponse){
            var list = new List<string>(1) { MockResponse };
            return getPaymentApiController(list);
        }

        protected GatewayApiConfig getGatewayApiConfig(){
            var gatewayApiConfig = new GatewayApiConfig();

            gatewayApiConfig.GatewayUrl = "http://gateway.com";
            gatewayApiConfig.Version = "45";
            gatewayApiConfig.MerchantId = "TEST_merchant";
            return gatewayApiConfig;
        }

        protected GatewayApiRequest getGatewayApiRequest(){
            return new GatewayApiRequest(getGatewayApiConfig())
            {
                SessionId = "SessionId",
                OrderId = IdUtils.generateSampleId(),
                TransactionId = IdUtils.generateSampleId(),
                ApiOperation = "OPERATION_MOCK",
                OrderAmount = "5000",
                OrderCurrency = "CURRENCY",
                OrderDescription = "Wonderful product that you should buy!"
            };
        }


        protected String getSecureIdMockResponse(String status){
            return  "{" +
                    "\"3DSecure\" : {" + 
                            "\"summaryStatus\" : \"" + status + "\" ," +
                        "\"authenticationRedirect\" : {" +
                            "\"customized\" : {" +
                                 "\"acsUrl\" : \"acsurl123\", " +
                                 "\"paReq\" : \"paReq123\" }" +
                            "}" +
                        "} " +
                    "}";
        }

        protected String getSessionMockResponse(){
            return "{ \"session\" : { \"id\" : \"id-123\" }, \"successIndicator\" : \"successIndicator-123\" }";
        }

        protected String getSessionCheckoutMockResponse(){
            return "{ " +
                                "\"merchant\": \"MERCHANT\", " +
                                "\"result\": \"SUCCESS\", " +
                                "\"session\": { " +
                                                "\"id\": \"SESSION123\", " +
                                                "\"updateStatus\": \"SUCCESS\", " +
                                                "\"version\": \"version123\" " +
                                              "}, " +
                                "\"successIndicator\": \"successIndicator123\" " +
                "} ";
        }

        protected String getInitiateBrowserMockResponse(String redirectURL, String result){
            return  "{\"browserPayment\":" +
                        "{\"interaction\":" +
                            "{\"status\":\"INITIATED\"," +
                             "\"timeInitiated\":\"2012-02-28T12:06:08.614Z\"}," +
                             "\"operation\":\"OPERATION\"," +
                              "\"nameOfSourcePayment\":{\"displayShippingAddress\":true," +
                              "\"overrideShippingAddress\":true," +
                              "\"paymentConfirmation\":\"CONFIRM_AT_PROVIDER\"}," +
                                "\"redirectUrl\":\"" + redirectURL + "\"," +
                               "\"returnUrl\":\"https://return.url\"}," +
                                "\"gatewayEntryPoint\":\"WEB_SERVICES_API\"," +
                                "\"merchant\":\"merchant123\"," +
                                "\"order\":" +
                                "{\"amount\":5000.00," +
                                "\"creationTime\":\"2012-02-28T12:06:08.605Z\"," +
                                "\"currency\":\"USD\"," +
                                "\"id\":\"id123\"," +
                                "\"status\":\"INITIATED\"," +
                                "\"totalAuthorizedAmount\":0," +
                                "\"totalCapturedAmount\":0," +
                                "\"totalRefundedAmount\":0}," +
                                "\"response\":{\"gatewayCode\":" +
                                "\"SUBMITTED\"}," +
                                "\"result\":\"" + result + "\"," +
                                "\"sourceOfFunds\":{\"type\":\"nameOfSOurcePayment\"}," +
                                "\"timeOfRecord\":\"2012-02-28T12:06:08.605Z\"," +
                                "\"transaction\":" +
                                "{\"acquirer\":" +
                                "{\"date\":\"2018-02-28\"," +
                                "\"id\":\"IDSOURCEPAYMENT\"," +
                                "\"merchantId\":\"merchantid123\"," +
                                "\"time\":\"12:06:08\"}," +
                                "\"amount\":5000.00," +
                                "\"currency\":\"USD\"," +
                                "\"frequency\":\"SINGLE\"," +
                                "\"id\":\"b932112d98\"," +
                                "\"source\":\"INTERNET\"," +
                                "\"type\":\"PAYMENT\"}," +
                                "\"version\":\"45\"}";
        }

        //TESTS
        [Fact]
        public void processTest(){
            String mockResponse = getSessionMockResponse();
            var paymentApiController = getPaymentApiController(mockResponse);

            GatewayApiRequest gatewayApiRequest = getGatewayApiRequest();
            Assert.True(this.checkPageAgainstActionResult(PAGE_API_VIEW, paymentApiController.Process(gatewayApiRequest)));
        }

        [Fact]
        public void ProcessHostedSessionTest()
        { 
            String mockResponse = getSessionMockResponse();
            var paymentApiController = getPaymentApiController(mockResponse);

            GatewayApiRequest gatewayApiRequest = getGatewayApiRequest();
            Assert.True(this.checkPageAgainstActionResult(PAGE_API_VIEW, paymentApiController.ProcessHostedSession(gatewayApiRequest)));
        }

        [Fact]
        public void hostedCheckoutReceiptTest()
        {
            String mockResponse = "{" +
                                    " \"transaction\":[{ " +
                                        "\"response\":{ \"acquirerCode\":\"Success\", \"gatewayCode\":\"APPROVED\" } ," +
                                        "\"result\":\"SUCCESS\", " +
                                       " \"order\":{" +
                                            "\"amount\":5000, " +
                                            "\"creationTime\":\"2012-02-28T13:20:03.897Z\"," +
                                            "\"currency\":\"USD\"," +
                                            "\"id\":\"id123\", " +
                                            "\"status\":\"CAPTURED\"," +
                                            "\"totalAuthorizedAmount\":5000," +
                                            "\"totalCapturedAmount\":5000," +
                                            "\"totalRefundedAmount\":0" +
                                            "}" +
                                    "}]" +
                                    "}";
            var paymentApiController = getPaymentApiController(mockResponse);
            paymentApiController.ControllerContext = getControllerContext();

            GatewayApiRequest gatewayApiRequest = getGatewayApiRequest();

            //success
            Assert.True(this.checkPageAgainstActionResult(PAGE_RECEIPT, paymentApiController.HostedCheckoutReceipt(IdUtils.generateSampleId(), "SUCCESS", "SESSIONID")));

            //error
            Assert.True(this.checkPageAgainstActionResult(PAGE_ERROR, paymentApiController.HostedCheckoutReceipt(IdUtils.generateSampleId(), "FAIL", "SESSIONID")));
        }

        [Fact]
        public void Check3dsEnrollmentTest()
        { 
            List<String> listOfMockResponse = new List<string>();

            String checkoutSessionModelMockResponse = getSessionCheckoutMockResponse();

            //success
            String secureIdMockResponseSuccess = getSecureIdMockResponse("CARD_ENROLLED");
            listOfMockResponse.Add(checkoutSessionModelMockResponse);
            listOfMockResponse.Add(secureIdMockResponseSuccess);

            var paymentApiController = getPaymentApiController(listOfMockResponse);

            Assert.True(  this.checkPageAgainstActionResult(PAGE_SECURE_ID, paymentApiController.Check3dsEnrollment(getGatewayApiRequest()))  );

            //error
            listOfMockResponse = new List<string>();

            String secureIdMockResponseError = getSecureIdMockResponse("FAIL");

            listOfMockResponse.Add(checkoutSessionModelMockResponse);
            listOfMockResponse.Add(secureIdMockResponseError);

            paymentApiController = getPaymentApiController(listOfMockResponse);

            Assert.True(this.checkPageAgainstActionResult(PAGE_ERROR, paymentApiController.Check3dsEnrollment(getGatewayApiRequest())));
        }


        [Fact]
        public void process3dsAuthenticationResultTest()
        {
            List<String> listOfMockResponse = new List<string>();
            listOfMockResponse.Add(getSecureIdMockResponse("SUCCESS"));
            listOfMockResponse.Add(getSessionMockResponse());
            var paymentApiController = getPaymentApiController(listOfMockResponse);

            Assert.True( this.checkPageAgainstActionResult(PAGE_API_VIEW, paymentApiController.Process3dsAuthenticationResult() )  );
        }



        [Fact]
        public void processPaymentWithPayPalTest()
        {
            String redirectURL = "https://redirect.url";
            //SUCCESS
            String initiateMockResponse = getInitiateBrowserMockResponse(redirectURL, "SUCCESS");
            var paymentApiController = getPaymentApiController(initiateMockResponse);
            GatewayApiRequest gatewayApiRequest = getGatewayApiRequest();
            Assert.True( this.checkPageAgainstActionResult(  redirectURL   , paymentApiController.ProcessPaymentWithPayPal(   gatewayApiRequest   )   )   );

            //ERROR
            initiateMockResponse = getInitiateBrowserMockResponse(redirectURL, "FAIL");
            paymentApiController = getPaymentApiController(initiateMockResponse);
            gatewayApiRequest = getGatewayApiRequest();
            Assert.True(this.checkPageAgainstActionResult(PAGE_ERROR, paymentApiController.ProcessPaymentWithPayPal(gatewayApiRequest)));
        }




        [Fact]
        public void browserPaymentReceiptTest(){
            String transactionId = IdUtils.generateSampleId();
            String orderId = IdUtils.generateSampleId();

            String retrieveOrderMockResponse = "{" +
                " \"transaction\":[{ " +
                    "\"response\":{ \"acquirerCode\":\"Success\", \"gatewayCode\":\"APPROVED\" } ," +
                    "\"result\":\"SUCCESS\", " +
                   " \"order\":{" +
                        "\"amount\":5000, " +
                        "\"creationTime\":\"2012-02-28T13:20:03.897Z\"," +
                        "\"currency\":\"USD\"," +
                        "\"id\":\"" + orderId + "\", " +
                        "\"status\":\"CAPTURED\"," +
                        "\"totalAuthorizedAmount\":5000," +
                        "\"totalCapturedAmount\":5000," +
                        "\"totalRefundedAmount\":0" +
                        "}" +
                "}]" +
                "}";

            var paymentApiController = getPaymentApiController(retrieveOrderMockResponse);
            Assert.True(  this.checkPageAgainstActionResult(  PAGE_RECEIPT, paymentApiController.browserPaymentReceipt(transactionId, orderId)   )   );
        }

        [Fact]
        public void processPaymentWithUnionPayTest()
        {
            String redirectURL = "https://redirect.url";
            //SUCCESS
            String initiateMockResponse = getInitiateBrowserMockResponse(redirectURL, "SUCCESS");
            var paymentApiController = getPaymentApiController(initiateMockResponse);
            GatewayApiRequest gatewayApiRequest = getGatewayApiRequest();
            Assert.True(this.checkPageAgainstActionResult(redirectURL, paymentApiController.ProcessPaymentWithUnionPay(gatewayApiRequest)));

            //ERROR
            initiateMockResponse = getInitiateBrowserMockResponse(redirectURL, "FAIL");
            paymentApiController = getPaymentApiController(initiateMockResponse);
            gatewayApiRequest = getGatewayApiRequest();
            Assert.True(this.checkPageAgainstActionResult(PAGE_ERROR, paymentApiController.ProcessPaymentWithUnionPay(gatewayApiRequest)));
        }


        [Fact]
        public void processPayThroughNVPTest(){
            var gatewayApiRequest = getGatewayApiRequest();
            gatewayApiRequest.NVPParameters = new Dictionary<string, string>();
            gatewayApiRequest.NVPParameters.Add("apiUsername", "username");
            gatewayApiRequest.NVPParameters.Add("apiPassword", "password");
            gatewayApiRequest.NVPParameters.Add("param1", "value1");

            String nvpResponseMock = "keyResponse1=value1&keyResponse2=value2";

            var paymentApiController = getPaymentApiController(nvpResponseMock);

            Assert.True(this.checkPageAgainstActionResult(PAGE_API_VIEW, paymentApiController.ProcessPayThroughNVP(gatewayApiRequest)));
        }


        [Fact]
        public void processMasterpassTest(){
            String masterpassWallteResponseMock = "{" +
               "\"wallet\" : {" +
               "\"masterpass\" : {" +
               "\"allowedCardTypes\" : \"card1,card2\" , " +
               "\"merchantCheckoutId\" : \"merchant132\", " +
               "\"originUrl\" : \"originurl\", " +
               "\"RequestToken\" : \"token\"" +
               "}" +
               "} " +
               "}";

            List<String> listOfMockResponse = new List<string>();
            listOfMockResponse.Add(getSessionCheckoutMockResponse());
            listOfMockResponse.Add("{}");
            listOfMockResponse.Add(masterpassWallteResponseMock);

            var paymentApiController = getPaymentApiController(listOfMockResponse);
            Assert.True( this.checkPageAgainstActionResult(  PAGE_MASTERPASS_BUTTON, paymentApiController.ProcessMasterpass(getGatewayApiRequest())  )  );
        }

        [Fact]
        public void processMasterpassResponseTest()
        {
            String masterpassTransactionResponseMock =  "{" +
                            "\"response\":{ \"acquirerCode\":\"Success\", \"gatewayCode\":\"APPROVED\" } ," +
                            "\"result\":\"SUCCESS\", " +
                           " \"order\":{" +
                                "\"amount\":5000, " +
                                "\"creationTime\":\"2012-02-28T13:20:03.897Z\"," +
                                "\"currency\":\"USD\"," +
                                "\"id\":\"id123\", " +
                                "\"status\":\"CAPTURED\"," +
                                "\"totalAuthorizedAmount\":5000," +
                                "\"totalCapturedAmount\":5000," +
                                "\"totalRefundedAmount\":0" +
                                "}" +
                            "}";

            List<String> listOfMockResponse = new List<string>();
            listOfMockResponse.Add("{}");
            listOfMockResponse.Add(masterpassTransactionResponseMock);

            var paymentApiController = getPaymentApiController(listOfMockResponse);
            Assert.True( this.checkPageAgainstActionResult( PAGE_RECEIPT, paymentApiController.masterpassResponse("token", "verifier", "checkoutId", "resource_url", "status")      )  );
        }


        [Fact]
        public void tokenizeTest()
        { 
            String tokenResponseMock = "{\"repositoryId\":\"respository\",\"response\":{\"gatewayCode\":\"BASIC_VERIFICATION_SUCCESSFUL\"},\"result\":\"SUCCESS\",\"sourceOfFunds\":{\"provided\":{\"card\":{\"brand\":\"CARDTYPE\",\"expiry\":\"9999\",\"fundingMethod\":\"CREDIT\",\"number\":\"000000xxxxxx0000\",\"scheme\":\"CARDTYPE\"}},\"type\":\"CARD\"},\"status\":\"VALID\",\"token\":\"TOKENNUMBER000\",\"usage\":{\"lastUpdated\":\"2018-03-08T11:34:58.457Z\",\"lastUpdatedBy\":\"TESTAB2894354\",\"lastUsed\":\"2018-03-08T10:52:37.979Z\"},\"verificationStrategy\":\"BASIC\"} ";

            List<String> listOfMockResponse = new List<string>();
            listOfMockResponse.Add("{}");
            listOfMockResponse.Add(tokenResponseMock);
            listOfMockResponse.Add("{}");

            var paymentApiController = getPaymentApiController(listOfMockResponse);
            var request = getGatewayApiRequest();
            request.SessionId = "SessionId";
            Assert.True(this.checkPageAgainstActionResult(PAGE_API_VIEW, paymentApiController.tokenize(request)));
        }


    }
}
