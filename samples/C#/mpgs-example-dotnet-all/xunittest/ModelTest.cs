using System;
using gateway_csharp_sample_code.Controllers;
using gateway_csharp_sample_code.Gateway;
using gateway_csharp_sample_code.Models;
using gateway_csharp_sample_code.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using Xunit;

namespace gateway_csharp_sample_code.xunittest
{

    /// <summary>
    /// Model Marshall test
    /// </summary>
    public class ModelTest
    {
        public ModelTest()
        {
        }

        /// <summary>
        /// Tests the IdUtil.
        /// </summary>
        [Fact]
        public void CheckOutSessionModelTest(){

            String message = "" +
                "{ " +
                    "\"merchant\": \"MERCHANT\", " +
                    "\"result\": \"SUCCESS\", " +
                    "\"session\": { " +
                                    "\"id\": \"SESSION123\", " +
                                    "\"updateStatus\": \"SUCCESS\", " +
                                    "\"version\": \"version123\" " +
                                  "}, " +
                    "\"successIndicator\": \"successIndicator123\" " +
                "} " ;

            CheckoutSessionModel model = CheckoutSessionModel.toCheckoutSessionModel(message);

            Assert.NotNull(model.Id);
            Assert.NotNull(model.SuccessIndicator);
            Assert.NotNull(model.Version);
        }

        /// <summary>
        /// Errors view model marshall test.
        /// </summary>
        [Fact]
        public void ErrorViewModelTest()
        { 
            String message = "{ \"error\":" +
                                "{ " +
                                   "\"cause\": \"cause123\", " +
                                   "\"field\": \"field123\", " +
                                   "\"explanation\": \"explanation123\", " +
                                   "\"validationType\": \"validationType123\" " +
                                  "} ," +
                                 "\"result\": \"result123\" " +
                               "}";
            String requestId = "requestId";
            ErrorViewModel model = ErrorViewModel.toErrorViewModel(requestId, message);

            Assert.True(model.RequestId == requestId);
            Assert.NotNull(model.Cause);
            Assert.NotNull(model.Field);
            Assert.NotNull(model.Explanation);
            Assert.NotNull(model.ValidationType);
            Assert.NotNull(model.Result);
        }

        /// <summary>
        /// Initiates the browser payment response model marshall test.
        /// </summary>
        /// 
        [Fact]
        public void InitiateBrowserPaymentResponseTest(){
            String message = "{\"browserPayment\":" +
                                    "{\"interaction\":" +
                                        "{\"status\":\"INITIATED\"," +
                                         "\"timeInitiated\":\"2012-02-28T12:06:08.614Z\"}," +
                                         "\"operation\":\"OPERATION\"," +
                                          "\"nameOfSourcePayment\":{\"displayShippingAddress\":true," +
                                          "\"overrideShippingAddress\":true," +
                                          "\"paymentConfirmation\":\"CONFIRM_AT_PROVIDER\"}," +
                                           "\"redirectUrl\":\"https://redirect.url\"," +
                                           "\"returnUrl\":\"https://return.url\"}," +
                "\"gatewayEntryPoint\":\"WEB_SERVICES_API\"," +
                "\"merchant\":\"merchant123\"," +
                "\"order\":" +
                "{\"amount\":5000.00," +
                "\"creationTime\":\"2012-02-28T12:06:08.605Z\"," +
                "\"currency\":\"USD\"," +
                "\"id\":\"id123\"," +
                "\"status\":\"INITIATED\"," +
                "\"totalAuthorizedAmount\":0," +
                "\"totalCapturedAmount\":0," +
                "\"totalRefundedAmount\":0}," +
                "\"response\":{\"gatewayCode\":" +
                "\"SUBMITTED\"}," +
                "\"result\":\"SUCCESS\"," +
                "\"sourceOfFunds\":{\"type\":\"nameOfSOurcePayment\"}," +
                "\"timeOfRecord\":\"2012-02-28T12:06:08.605Z\"," +
                "\"transaction\":" +
                "{\"acquirer\":" +
                "{\"date\":\"2018-02-28\"," +
                "\"id\":\"IDSOURCEPAYMENT\"," +
                "\"merchantId\":\"merchantid123\"," +
                "\"time\":\"12:06:08\"}," +
                "\"amount\":5000.00," +
                "\"currency\":\"USD\"," +
                "\"frequency\":\"SINGLE\"," +
                "\"id\":\"b932112d98\"," +
                "\"source\":\"INTERNET\"," +
                "\"type\":\"PAYMENT\"}," +
                "\"version\":\"45\"}";

            InitiateBrowserPaymentResponse model = InitiateBrowserPaymentResponse.toInitiateBrowserPaymentResponse(message);

            Assert.NotNull(model.RedirectUrl);
            Assert.NotNull(model.ResponseCode);
            Assert.NotNull(model.Result);
            Assert.NotNull(model.TransactionId);
            Assert.NotNull(model.OrderId);
            Assert.NotNull(model.InteractionStatus);
                    
        }


        //TARGETED
        /// <summary>
        /// Masterpasses the wallet response marshall test.
        /// </summary>
        [Fact]
        public void MasterpassWalletResponseTest()
        {

            String message = "{" +
                "\"wallet\" : {" +
                "\"masterpass\" : {" +
                "\"allowedCardTypes\" : \"card1,card2\" , " +
                "\"merchantCheckoutId\" : \"merchant132\", " +
                "\"originUrl\" : \"originurl\", " +
                "\"RequestToken\" : \"token\"" +
                "}" +
                "} " +
                "}";


            MasterpassWalletResponse model = MasterpassWalletResponse.toMasterpassWalletResponse(message);

            Assert.NotNull(model.AllowedCardTypes);
            Assert.NotNull(model.MerchantCheckoutId);
            Assert.NotNull(model.OriginUrl);
            Assert.NotNull(model.RequestToken);
        }


        /// <summary>
        /// Secures the identifier enrollment response model Marshall test.
        /// </summary>
        [Fact]
        public void SecureIdEnrollmentResponseModelTest(){

            String message = "{" +
                "\"3DSecure\" : {" +
                    "\"summaryStatus\" : \"summarystatus123\" ," +
                    "\"authenticationRedirect\" : {" +
                        "\"customized\" : {" +
                             "\"acsUrl\" : \"acsurl123\", " +
                             "\"paReq\" : \"paReq123\" }" +
                        "}" +
                    "} " +
                "}";

            //mock of request object
            var request = new Mock<HttpRequest>();

            request.Setup(x => x.Scheme).Returns("Scheme");
            request.Setup(x => x.Host).Returns(new HostString());
            request.Setup(x => x.PathBase).Returns("/Path");

            SecureIdEnrollmentResponseModel model = SecureIdEnrollmentResponseModel.toSecureIdEnrollmentResponseModel(request.Object, message);

            Assert.NotNull(model.MdValue);
            Assert.NotNull(model.PaReq);
            Assert.NotNull(model.ResponseUrl);
            Assert.NotNull(model.Status);
            Assert.NotNull(model.AcsUrl);
        }


        /// <summary>
        /// Transactions the response model marshall test.
        /// </summary>
        [Fact]
        public void TransactionResponseModelTest()
        {
            String message = "{" +
                " \"transaction\":[{ " +
                    "\"response\":{ \"acquirerCode\":\"Success\", \"gatewayCode\":\"APPROVED\" } ," +
                    "\"result\":\"SUCCESS\", " +
                   " \"order\":{" +
                        "\"amount\":5000, " +
                        "\"creationTime\":\"2012-02-28T13:20:03.897Z\"," +
                        "\"currency\":\"USD\"," +
                        "\"id\":\"id123\", " +
                        "\"status\":\"CAPTURED\"," +
                        "\"totalAuthorizedAmount\":5000," +
                        "\"totalCapturedAmount\":5000," +
                        "\"totalRefundedAmount\":0" +
                        "}" +
                "}]" +
                "}";

            TransactionResponseModel model = TransactionResponseModel.toTransactionResponseModel(message);


            Assert.NotNull(model.ApiResult);
            Assert.NotNull(model.GatewayCode);
            Assert.NotNull(model.OrderAmount);
            Assert.NotNull(model.OrderCurrency);
            Assert.NotNull(model.OrderId);
        }

        /// <summary>
        /// TokensResponse model marshall test.
        /// </summary>
        [Fact]
        public void TokenResponseModelTest(){
            String message = "{\"repositoryId\":\"respository\",\"response\":{\"gatewayCode\":\"BASIC_VERIFICATION_SUCCESSFUL\"},\"result\":\"SUCCESS\",\"sourceOfFunds\":{\"provided\":{\"card\":{\"brand\":\"CARDTYPE\",\"expiry\":\"9999\",\"fundingMethod\":\"CREDIT\",\"number\":\"000000xxxxxx0000\",\"scheme\":\"CARDTYPE\"}},\"type\":\"CARD\"},\"status\":\"VALID\",\"token\":\"TOKENNUMBER000\",\"usage\":{\"lastUpdated\":\"2018-03-08T11:34:58.457Z\",\"lastUpdatedBy\":\"TESTAB2894354\",\"lastUsed\":\"2018-03-08T10:52:37.979Z\"},\"verificationStrategy\":\"BASIC\"} ";

            var model = TokenResponse.ToTokenResponse(message);

            Assert.NotNull(model.GatewayCode);
            Assert.NotNull(model.Result);
            Assert.NotNull(model.Status);
            Assert.NotNull(model.Token);
        }

    }
}
