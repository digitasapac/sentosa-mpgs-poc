using System;
using gateway_csharp_sample_code.Controllers;
using gateway_csharp_sample_code.Gateway;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using Xunit;

namespace gateway_csharp_sample_code.xunittest
{

    /// <summary>
    /// Bundle package test
    /// </summary>
    public class DistTest : BaseTest
    {
        //ESSENTIALS
        private readonly string PAGE_PAY = "Pay";
        private readonly string PAGE_AUTHORIZE = "Authorize";
        private readonly string PAGE_CAPTURE = "Capture";
        private readonly string PAGE_REFUND = "Refund";
        private readonly string PAGE_VOID = "Void";
        private readonly string PAGE_VERIFY = "Verify";
        private readonly string PAGE_RETRIEVE = "RetrieveOrder";
        private readonly string PAGE_HOSTED = "HostedCheckout";
        private readonly string PAGE_3DS = "PayWith3ds";
        private readonly string PAGE_NVP = "PayThroughNVP";
        private readonly string PAGE_PAYTOKEN = "PayWithToken";
        private readonly string PAGE_WEBHOOKS = "Webhooks";


        //TARGETED
        private readonly string PAGE_UNIONPAY = "PayWithUnionPay";
        private readonly string PAGE_MASTERPASS= "PayWithMasterpass";


        //ALL
        private readonly string PAGE_PAYPAL = "PayWithPayPal";





        private PaymentController paymentController;
        private WebhooksController webhooksController;


        public DistTest()
        {
            var gatewayApiConfig = new GatewayApiConfig();

            gatewayApiConfig.GatewayUrl = "http://gateway.com";
            gatewayApiConfig.Version = "45";
            gatewayApiConfig.MerchantId = "TEST_merchant";

            var ioptions = new Mock<IOptions<GatewayApiConfig>>();
            ioptions.Setup(op => op.Value).Returns(gatewayApiConfig);

            var loggerGatewayApi = new Mock<ILogger<gateway_csharp_sample_code.Gateway.GatewayApiClient>>();
            var loggerNVPClient = new Mock<ILogger<gateway_csharp_sample_code.Gateway.NVPApiClient>>();
            var loggerPayment = new Mock<ILogger<gateway_csharp_sample_code.Controllers.PaymentController>>();
            var loggerWebhooks = new Mock<ILogger<gateway_csharp_sample_code.Controllers.WebhooksController>>();



            var gatewayApiClient = new Mock<GatewayApiClient>(ioptions.Object, loggerGatewayApi.Object);
            gatewayApiClient.Setup( o => o.SendTransaction(It.IsNotNull<GatewayApiRequest>())).Returns("{ \"session\" : { \"id\" : \"id-123\" }, \"successIndicator\" : \"successIndicator-123\" }");

            var nvpApiClient = new NVPApiClient(ioptions.Object, loggerNVPClient.Object);

            paymentController = new PaymentController(ioptions.Object, gatewayApiClient.Object, nvpApiClient, loggerPayment.Object);
            paymentController.ControllerContext = getControllerContext();
            webhooksController = new WebhooksController(ioptions.Object, loggerWebhooks.Object);
        }


        [Fact]
        public void TestEssentials(){
            Console.WriteLine("Essentials bundle checking...");

            //Index
            Assert.True(checkPageAgainstActionResult(PAGE_PAY, paymentController.Index()));

            //Pay
            Assert.True(checkPageAgainstActionResult(PAGE_PAY, paymentController.ShowPay()));

            //Pay with token
            Assert.True(checkPageAgainstActionResult(PAGE_PAYTOKEN, paymentController.ShowPayWithToken()));

            //Authorize
            Assert.True(checkPageAgainstActionResult(PAGE_AUTHORIZE, paymentController.ShowAuthorize()));

            //Capture
            Assert.True(checkPageAgainstActionResult(PAGE_CAPTURE, paymentController.ShowCapture()));

            //refund
            Assert.True(checkPageAgainstActionResult(PAGE_REFUND, paymentController.ShowRefund()));

            //void
            Assert.True(checkPageAgainstActionResult(PAGE_VOID, paymentController.ShowVoid()));

            //verify
            Assert.True(checkPageAgainstActionResult(PAGE_VERIFY, paymentController.ShowVerify()));

            //retrieve
            Assert.True(checkPageAgainstActionResult(PAGE_RETRIEVE, paymentController.ShowRetrieveOrder()));

            //hosted
            Assert.True(checkPageAgainstActionResult(PAGE_HOSTED, paymentController.ShowHostedCheckout()));

            //3ds
            Assert.True(checkPageAgainstActionResult(PAGE_3DS, paymentController.ShowPayWith3ds()));

            //nvp
            Assert.True(checkPageAgainstActionResult(PAGE_NVP, paymentController.ShowPayThroughNVP()));

            //webhooks
            Assert.True(checkPageAgainstActionResult(PAGE_WEBHOOKS, webhooksController.ShowWebhooks()));


            Console.WriteLine("Essentials bundle checking done...");
        }



        [Fact]
        public void TestTargeted()
        {
            Console.WriteLine("Targeted package checking...");

            //UnionPay
            Assert.True(checkPageAgainstActionResult(PAGE_UNIONPAY, paymentController.ShowPayWithUnionPaySecurePay()));

            //masterpass
            Assert.True(checkPageAgainstActionResult(PAGE_MASTERPASS, paymentController.ShowMasterpass()));


            Console.WriteLine("Targeted package checking done...");

        }


        [Fact]
        public void TestAll()
        {
            Console.WriteLine("All bundle checking...");

            //Paypal
            Assert.True(checkPageAgainstActionResult(PAGE_PAYPAL, paymentController.ShowPayWithPaypal()));

            Console.WriteLine("All bunbdle checking done...");
        }


        

    }
}
